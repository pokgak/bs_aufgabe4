# BSP Aufgabe 4 WS17/18

Write to device with `echo $STRING > /dev/tzm$DEVNO` where
$STRING is the string you want to write to the device and 
$DEVNO the device number you want to write to.  
Example: 
    echo abc > /dev/tzm0
writes the string 'abc' to `/dev/tzm0`.  

Read from device with `dd if=/dev/tzm$DEVNO` where $DEVNO same as before. 
`dd` reads infinitely from device if `count` not given.  
Example: 
    dd if=/dev/tzm0 count=1
to read once from device. Without count `dd` reads until `CTRL+C` is detected.  

Compile with `make`.  
After compiling, reload the new compiled module with
    sudo ./tzm.init

