/*
 * pipe.c -- fifo driver for tzm
 *
 * Copyright (C) 2001 Alessandro Rubini and Jonathan Corbet
 * Copyright (C) 2001 O'Reilly & Associates
 *
 * The source code in this file can be freely used, adapted,
 * and redistributed in source or binary form, so long as an
 * acknowledgment appears in derived source files.  The citation
 * should list that the code comes from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   No warranty is attached;
 * we cannot take responsibility for errors or fitness for use.
 *
 */
 
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/kernel.h>	/* printk(), min() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/cdev.h>
#include <asm/uaccess.h>	/* copy_to/from_user */
#include <linux/jiffies.h>	/* Measure time with jiffies */
#include <linux/mutex.h>

#include "tzm.h"		/* local definitions */

/* parameters */
int tzm_major =   TZM_MAJOR_DYNAMIC;
int tzm_minor =   0;
static int tzm_nr_devs = TZM_NR_DEVS;	/* number of tzm devices */
dev_t tzm_devno;			/* Our first device number */
static int is_read_open = 0;
static int is_write_open = 0;
static int ret_val_time = -1;
static int ret_val_number = -1;
static u64 time_last_input = 0;

/* Permission 0 (last parameter in module_param) because we do not need read/write access outside this module */
module_param(ret_val_time, int, 0);
module_param(ret_val_number, int, 0);
MODULE_LICENSE("GPL");

static struct mutex mutex_ret_val;
static struct cdev tzm_cdev;

/*
 * Open and close
 */


static int tzm_open(struct inode *inode, struct file *filp)
{
	mutex_lock(&mutex_ret_val);

	/* use f_mode,not  f_flags: it's cleaner (fs/open.c tells why) */
	if (filp->f_mode & FMODE_READ){
		if (is_read_open){
			mutex_unlock(&mutex_ret_val);
		        return -EBUSY;
	    	}
		is_read_open = 1;
    	}
    
	if (filp->f_mode & FMODE_WRITE){
		if (is_write_open){
			mutex_unlock(&mutex_ret_val);
		        return -EBUSY;
	    	}
		is_write_open= 1;
    	}
    
	mutex_unlock(&mutex_ret_val);
	return 0; /* succeed */
}



static int tzm_release(struct inode *inode, struct file *filp)
{
	mutex_lock(&mutex_ret_val);
	if (filp->f_mode & FMODE_READ)
		is_read_open = 0;
	if (filp->f_mode & FMODE_WRITE)
		is_write_open = 0;
	mutex_unlock(&mutex_ret_val);
	return 0;
}


/*
 * Data management: read and write
 */

static ssize_t tzm_read (struct file *filp, char __user *buf, size_t count,
                loff_t *f_pos)
{
	char ret_string[50] = {0}; /* initialize ret_string and fill with 0 */
	size_t bytes_not_copied;

	mutex_lock(&mutex_ret_val);
 
	/* Only count time difference if there is input already.
	 * If not, output the default value -1.
	 * */
	if (time_last_input > 0)
		/* HZ = Hertz; This converts jiffies to seconds.
		 * See https://stackoverflow.com/a/2738058 
		 **/
		ret_val_time = (get_jiffies_64() - time_last_input) / HZ; 

	snprintf(ret_string, sizeof(ret_string), "Number char: %d; Timer: %d\n",
			ret_val_number, ret_val_time);

	PDEBUG("String to print: %s", ret_string);

	bytes_not_copied = copy_to_user(buf, ret_string, strlen(ret_string));

	PDEBUG("bytes_not_copied: %zu", bytes_not_copied);

	mutex_unlock(&mutex_ret_val);

	return count - bytes_not_copied;
}

static int count_char(char *input) {
	char *i;
	int count = 0;

	for (i = input; *i && *i != '\n'; i++) {
		PDEBUG("checking char %c\n", *i);

		count++;
	}

	return count;
}

static ssize_t tzm_write(struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos)
{
	char buffer[count];

	mutex_lock(&mutex_ret_val);

	/* save time */
	time_last_input = get_jiffies_64();

	if (copy_from_user(buffer, buf, count)) {
		PDEBUG("cannot copy all input to kernel buffer. Exiting");
		mutex_unlock(&mutex_ret_val);
		return -EFAULT;
	}

	ret_val_number = count_char(buffer);

	PDEBUG("count is %d", ret_val_number);

	mutex_unlock(&mutex_ret_val);
	return count;
}


/*
 * The file operations for the pipe device
 * (some are overlayed with bare tzm)
 */
struct file_operations tzm_dev_fops = {
	.owner =	THIS_MODULE,
	.read =		tzm_read,
	.write =	tzm_write,
	.open =		tzm_open,
	.release =	tzm_release,
};



/*
 * Initialize the pipe devs; return how many we did.
 */
int tzm_init(void)
{
	int result;
	int err;
	tzm_devno = 0;

	result = alloc_chrdev_region(&tzm_devno, tzm_minor, tzm_nr_devs, "tzm");
      	tzm_major = MAJOR(tzm_devno);
        
	if (result < 0) {
		PDEBUG("Unable to get tzm region, error %d\n", result);
		return 0;
	}
	
    	mutex_init(&mutex_ret_val);
    
	cdev_init(&tzm_cdev, &tzm_dev_fops);
	tzm_cdev.owner = THIS_MODULE;
	err = cdev_add (&tzm_cdev, tzm_devno, 1);
    
	/* Fail gracefully if need be */
	if (err)
		PDEBUG("Error %d adding tzm0", err);
	
	return 0; /* succeed */
}

/*
 * This is called by cleanup_module or on failure.
 * It is required to never fail, even if nothing was initialized first
 */
void tzm_cleanup(void)
{
    	cdev_del(&tzm_cdev);
	unregister_chrdev_region(tzm_devno, tzm_nr_devs);
	mutex_trylock(&mutex_ret_val);
	mutex_unlock(&mutex_ret_val);
	mutex_destroy(&mutex_ret_val);
}

module_init(tzm_init);
module_exit(tzm_cleanup);
